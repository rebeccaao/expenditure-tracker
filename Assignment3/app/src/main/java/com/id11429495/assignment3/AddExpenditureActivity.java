package com.id11429495.assignment3;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.id11429495.assignment3.model.ExpenditureData;
import com.id11429495.assignment3.model.ExpenditureDatabaseHelper;

import java.io.File;
import java.util.Calendar;

/**
 * Activity to enter a new Expenditure to the database
 */
public class AddExpenditureActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mDateEditText;
    private EditText mAmountEditText;
    private EditText mRecipientEditText;
    private EditText mCategoryEditText;
    private EditText mDescriptionEditText;
    private String mImageFilepath;
    private DatePickerDialog mDatePickerDialog;
    private Button mAddPhotoButton;
    private Button mDeletePhotoButton;
    private ImageView mReceiptImageView;

    private Uri mImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_add_expenditure);
        setViewsAndListeners();
        setDateField();
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_expenditure, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            //return to previous activity. delete the photo from phone memory
            case android.R.id.home:
                deletePhoto();
                finish();
                break;
            //submits form data
            case R.id.add_expenditure_menu_submit:
                onSubmitClicked();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * sets views from XML layout and sets OnClickListeners to buttons
     */
    private void setViewsAndListeners() {
        mDateEditText = (EditText) findViewById(R.id.add_expenditure_date_edit_text);
        mAmountEditText = (EditText) findViewById(R.id.add_expenditure_amount_edit_text);
        mRecipientEditText = (EditText) findViewById(R.id.add_expenditure_recipient_edit_text);
        mCategoryEditText = (EditText) findViewById(R.id.add_expenditure_category_edit_text);
        mDescriptionEditText = (EditText) findViewById(R.id.add_expenditure_description_edit_text);
        mAddPhotoButton = (Button) findViewById(R.id.add_expenditure_add_photo_button);
        mDeletePhotoButton = (Button) findViewById(R.id.add_expenditure_delete_photo_button);
        mReceiptImageView = (ImageView) findViewById(R.id.add_expenditure_photos_view);

        mDateEditText.setOnClickListener(this);
        mCategoryEditText.setOnClickListener(this);
        mAddPhotoButton.setOnClickListener(this);
        mDeletePhotoButton.setOnClickListener(this);
    }

    /**
     * handle OnClickListener button clicks
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_expenditure_date_edit_text:
                mDatePickerDialog.show();
                break;
            case R.id.add_expenditure_category_edit_text:
                showCategorySpinner();
                break;
            case R.id.add_expenditure_add_photo_button:
                takePhoto();
                break;
            case R.id.add_expenditure_delete_photo_button:
                deletePhoto();
                break;
        }
    }

    /**
     * create dialog for selecting date field in form
     */
    public void setDateField() {
        Calendar newCalendar = Calendar.getInstance();
        //create dialog and sets date EditText with chosen date
        mDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mDateEditText.setText(Constants.DATE_FORMAT.format(newDate.getTime()));
            }
            //set to today by default
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * spinner for category EditText with categories from strings.xml
     */
    private void showCategorySpinner() {
        final String[] categories = getResources().getStringArray(R.array.categories_array);
        final ArrayAdapter<String> categoriesSpinner = new ArrayAdapter<>(AddExpenditureActivity.this, android.R.layout.simple_spinner_dropdown_item, categories);
        new AlertDialog.Builder(AddExpenditureActivity.this).setTitle("Category").setAdapter(categoriesSpinner, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                //sets category EditText to chosen category
                mCategoryEditText.setText(categories[item]);
                dialog.dismiss();
            }
        }).create().show();
    }

    /**
     * starts activity to take a photo using the camera
     */
    private void takePhoto() {
        ContentValues values = new ContentValues();
        mImageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        startActivityForResult(intent, Constants.REQUEST_IMAGE_CAPTURE);
    }

    /**
     * handles methods for after an startActivityForResult is executed
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //handles ActivityResult for REQUEST_IMAGE_CAPTURE with the camera
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = managedQuery(mImageUri, projection, null, null, null);
            int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            mImageFilepath = cursor.getString(column_index_data);

            Bitmap cameraImage = Methods.decodeSampledBitmapFromFile(mImageFilepath);
            mReceiptImageView.setImageBitmap(cameraImage);

            mAddPhotoButton.setVisibility(View.GONE);
            mDeletePhotoButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * delete the photo from the gallery and reset the ImageView and buttons
     */
    private void deletePhoto() {
        if (mImageFilepath != null && !mImageFilepath.isEmpty()) {
            File file = new File(mImageFilepath);
            file.delete();

            mReceiptImageView.setImageBitmap(null);
            mAddPhotoButton.setVisibility(View.VISIBLE);
            mDeletePhotoButton.setVisibility(View.GONE);
        }
    }

    /**
     * handle when submit button is clicked.
     * data from inputs will be added to the database and result is sent to previous activity
     */
    public void onSubmitClicked() {
        //check validity of inputs before adding to database
        if (inputsAreValid()) {
            //get all input data
            long date = Methods.getDateInMilliseconds(mDateEditText.getText().toString());
            float amount = Float.parseFloat(mAmountEditText.getText().toString());
            String recipient = mRecipientEditText.getText().toString();
            String category = mCategoryEditText.getText().toString();
            String description = mDescriptionEditText.getText().toString();

            //send ExpenditureData to database or Log error if something occurs
            try {
                ExpenditureData expenditureData = new ExpenditureData(0, date, amount, recipient, category, description, mImageFilepath);
                ExpenditureDatabaseHelper.get(this, ExpenditureData.DATABASE_NAME).addExpenditure(expenditureData);
            } catch (Exception ex) {
                Log.e(Constants.TAG, "onSubmitClicked Exception:", ex);
            }

            //create intent to send OK result and specified date from input to previous activity
            Intent intent = new Intent();
            intent.putExtra(Constants.SELECTED_DATE, date);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    /**
     * check all the required EditTexts contain data suitable for the database
     *
     * @return
     */
    private boolean inputsAreValid() {
        EditText[] requiredEditTexts = new EditText[]{mAmountEditText, mDateEditText, mRecipientEditText, mCategoryEditText};
        boolean check = true;
        for (EditText editText : requiredEditTexts) {
            if (!Methods.hasText(editText)) {
                check = false;
            }
        }
        return check;
    }
}
