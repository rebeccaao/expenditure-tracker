package com.id11429495.assignment3.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.id11429495.assignment3.Constants;
import com.id11429495.assignment3.R;
import com.id11429495.assignment3.ViewExpenditureSingleActivity;
import com.id11429495.assignment3.model.ExpenditureCursor;
import com.id11429495.assignment3.model.ExpenditureData;
import com.id11429495.assignment3.model.ExpenditureDatabaseHelper;

/**
 * Adapter to create and popular ListView to display Expenditures for the specified date
 */
public class ListExpendituresByDateAdapter extends CursorAdapter {
    Resources mResources;
    String mPackage;

    public ListExpendituresByDateAdapter(Context context, long date) {
        super(context, ExpenditureDatabaseHelper.get(context, ExpenditureData.DATABASE_NAME).getExpendituresByDate(date), 0);
        mResources = context.getResources();
        mPackage = context.getPackageName();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expenditure_list_data, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ExpenditureData expenditureData = ((ExpenditureCursor) cursor).getExpenditureData();
        final Context thisContext = context;

        RelativeLayout expenditureLayout = (RelativeLayout) view.findViewById(R.id.list_expenditure_layout);
        TextView recipientTextView = (TextView) view.findViewById(R.id.list_expenditure_recipient);
        TextView amountTextView = (TextView) view.findViewById(R.id.list_expenditure_amount);

        // change background color of expenditure according to category
        GradientDrawable expenditureLayoutBackground = (GradientDrawable) expenditureLayout.getBackground();
        int categoryColorResId = mResources.getIdentifier(expenditureData.getCategory().toLowerCase(), "color", mPackage);
        expenditureLayoutBackground.setColor(mResources.getColor(categoryColorResId));

        recipientTextView.setText(expenditureData.getRecipient());
        String amountInCurrency = Constants.CURRENCY_FORMAT.format(expenditureData.getAmount());
        amountTextView.setText(String.format(mResources.getString(R.string.formatted_amount), amountInCurrency));

        //open full details page of expenditure in ViewExpenditureSingleActivity
        expenditureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(thisContext.getApplicationContext(), ViewExpenditureSingleActivity.class);
                intent.putExtra(Constants.EXPENDITURE_DATA, expenditureData);
                thisContext.startActivity(intent);
            }
        });
    }
}