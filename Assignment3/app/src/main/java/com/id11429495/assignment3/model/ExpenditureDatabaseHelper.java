package com.id11429495.assignment3.model;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.File;
import java.util.ArrayList;

/**
 * Database helper for accessing expediture data from the database
 */
public class ExpenditureDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static ExpenditureDatabaseHelper instance;

    /**
     * access singleton instance of the class
     *
     * @param context
     * @param databaseName
     * @return
     */
    public static synchronized ExpenditureDatabaseHelper get(Context context, String databaseName) {
        if (instance == null) {
            instance = new ExpenditureDatabaseHelper(context.getApplicationContext(), databaseName);
        }
        return instance;
    }

    /**
     * create ExpenditureDatabaseHelper
     *
     * @param context
     * @param databaseName
     */
    private ExpenditureDatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, new ExpenditureCursorFactory(), DATABASE_VERSION);
    }

    /**
     * create the ExpenditureData table
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createExpenditureSql = "create table " + ExpenditureData.TABLE + " ("
                + ExpenditureData.COLUMN_ID + " integer primary key autoincrement, "
                + ExpenditureData.COLUMN_DATE + " integer not null, "
                + ExpenditureData.COLUMN_AMOUNT + " float not null, "
                + ExpenditureData.COLUMN_RECIPIENT + " text not null, "
                + ExpenditureData.COLUMN_CATEGORY + " text not null, "
                + ExpenditureData.COLUMN_DESCRIPTION + " text,"
                + ExpenditureData.COLUMN_RECEIPTS + " text)";
        db.execSQL(createExpenditureSql);
    }

    /**
     * called for when DATABASE_VERSION is changed
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * add a new ExpenditureData row to the database
     *
     * @param expenditureData
     */
    public void addExpenditure(ExpenditureData expenditureData) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ExpenditureData.COLUMN_DATE, expenditureData.getDate());
        values.put(ExpenditureData.COLUMN_AMOUNT, expenditureData.getAmount());
        values.put(ExpenditureData.COLUMN_RECIPIENT, expenditureData.getRecipient());
        values.put(ExpenditureData.COLUMN_CATEGORY, expenditureData.getCategory());
        values.put(ExpenditureData.COLUMN_DESCRIPTION, expenditureData.getDescription());
        values.put(ExpenditureData.COLUMN_RECEIPTS, expenditureData.getReceipt());
        db.insertOrThrow(ExpenditureData.TABLE, null, values);
    }

    /**
     * get a cursor of all ExpenditureData in the database
     *
     * @return
     */
    public ExpenditureCursor getAllExpenditures() {
        SQLiteDatabase db = getReadableDatabase();
        ExpenditureCursor c = (ExpenditureCursor) db.rawQuery("SELECT * FROM " + ExpenditureData.TABLE, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * return a cursor of all ExpenditureData with receipt filepaths
     *
     * @return
     */
    public ExpenditureCursor getAllExpendituresWithReceipts() {
        SQLiteDatabase db = getReadableDatabase();
        ExpenditureCursor c = (ExpenditureCursor) db.rawQuery("SELECT *"
                + " FROM " + ExpenditureData.TABLE
                + " WHERE " + ExpenditureData.COLUMN_RECEIPTS + " IS NOT NULL", null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * return a cursor of dates and accumulated amounts based on category
     *
     * @param category
     * @return
     */
    public ExpenditureCursor getExpendituresAmountByCategory(String category) {
        SQLiteDatabase db = getReadableDatabase();
        ExpenditureCursor c = (ExpenditureCursor) db.rawQuery("SELECT "
                + ExpenditureData.COLUMN_DATE + ", SUM(" + ExpenditureData.COLUMN_AMOUNT + ") as " + ExpenditureData.COLUMN_ACCUMULATED_AMOUNT
                + " FROM " + ExpenditureData.TABLE
                + " WHERE " + ExpenditureData.COLUMN_CATEGORY + "='" + category + "'"
                + " GROUP BY " + ExpenditureData.COLUMN_DATE, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * return a cursor for all rows with the specified date
     *
     * @param date
     * @return
     */
    public ExpenditureCursor getExpendituresByDate(long date) {
        SQLiteDatabase db = getReadableDatabase();
        ExpenditureCursor c = (ExpenditureCursor) db.rawQuery("SELECT * FROM " + ExpenditureData.TABLE + " WHERE " + ExpenditureData.COLUMN_DATE + "=" + date, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * check if the database has data by creating a cursor and checking if it has value
     *
     * @return
     */
    public boolean hasData() {
        SQLiteDatabase db = getReadableDatabase();
        ExpenditureCursor c = (ExpenditureCursor) db.rawQuery("SELECT * FROM " + ExpenditureData.TABLE, null);
        return (c != null);
    }

    /**
     * custom cursor factory which created instances of cursors
     */
    private static class ExpenditureCursorFactory implements SQLiteDatabase.CursorFactory {
        @Override
        public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery, String editTable, SQLiteQuery query) {
            return new ExpenditureCursor(masterQuery, editTable, query);
        }
    }
}

