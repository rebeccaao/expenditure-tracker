package com.id11429495.assignment3.model;

import android.database.Cursor;

/**
 * Simple class containing data of accumulated amount spent by date.
 * used for displaying graph data in ViewGraphActivity
 */
public class ExpenditureGraphData {

    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TOTAL_AMOUNT = "accumulated_amount";

    private long mDate;
    private float mTotalAmount;

    /**
     * constructor to create ExpenditureGraphData object with data
     *
     * @param date
     * @param amount
     */
    public ExpenditureGraphData(long date, float amount) {
        this.mDate = date;
        this.mTotalAmount = amount;
    }

    /**
     * constructor to create a ExpenditureGraphData with data from a cursor
     *
     * @param cursor
     */
    public ExpenditureGraphData(Cursor cursor) {
        this.mDate = cursor.getLong(cursor.getColumnIndex(COLUMN_DATE));
        this.mTotalAmount = cursor.getFloat(cursor.getColumnIndex(COLUMN_TOTAL_AMOUNT));
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        this.mDate = date;
    }

    public float getTotalAmount() {
        return mTotalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.mTotalAmount = totalAmount;
    }
}
