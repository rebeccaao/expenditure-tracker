package com.id11429495.assignment3;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.melnykov.fab.FloatingActionButton;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

/**
 * An activity to show a calendar and send chosen date to ViewExpendituresListActivity to display data
 */
public class ViewCalendarActivity extends AppCompatActivity implements OnDateSelectedListener, View.OnClickListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private MaterialCalendarView mCalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_calendar);
        setViewsAndListeners();
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_calendar, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            //show navigation drawer
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * sets NavigationDrawer
     * sets views from XML layout and sets OnClickListeners to buttons
     */
    private void setViewsAndListeners() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.view_calendar_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.view_calendar_nav_drawer);
        Methods.setNavigationDrawer(this, mDrawerLayout, mDrawerList);

        FloatingActionButton addExpenditureButton = (FloatingActionButton) findViewById(R.id.add_expenditure_floating_button);
        addExpenditureButton.setOnClickListener(this);

        mCalendarView = (MaterialCalendarView) findViewById(R.id.calendar_view);
        mCalendarView.setOnDateChangedListener(this);
    }

    /**
     * handle OnClickListener button clicks
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.add_expenditure_floating_button:
                intent = new Intent(getApplicationContext(), AddExpenditureActivity.class);
                startActivityForResult(intent, Constants.ADD_ACTIVITY_REQUEST_CODE);
                break;
        }
    }

    /**
     * handles methods for after an startActivityForResult is executed
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ADD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent intent = new Intent(getApplicationContext(), ViewExpendituresListActivity.class);
            intent.putExtra(Constants.SELECTED_DATE, data.getLongExtra(Constants.SELECTED_DATE, Constants.DEFAULT_LONG_VALUE));
            startActivity(intent);
        }
    }

    /**
     * when a date is selected, send it to the ViewExpendituresListActivity to display data
     *
     * @param materialCalendarView
     * @param calendarDay
     * @param b
     */
    @Override
    public void onDateSelected(MaterialCalendarView materialCalendarView, CalendarDay calendarDay, boolean b) {
        Intent intent;
        intent = new Intent(getApplicationContext(), ViewExpendituresListActivity.class);
        intent.putExtra(Constants.SELECTED_DATE, getSelectedDatesLong());
        startActivity(intent);
    }

    /**
     * get selected date from calendar and convert to long
     *
     * @return
     */
    private long getSelectedDatesLong() {
        CalendarDay date = mCalendarView.getSelectedDate();
        return Methods.getDateInMilliseconds(Constants.DATE_FORMAT.format(date.getDate()));
    }
}
