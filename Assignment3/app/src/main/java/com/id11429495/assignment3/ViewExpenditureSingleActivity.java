package com.id11429495.assignment3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.id11429495.assignment3.model.ExpenditureData;
import com.ikimuhendis.ldrawer.ActionBarDrawerToggle;
import com.ikimuhendis.ldrawer.DrawerArrowDrawable;

import java.util.Date;

/**
 * Activity to display details for a single ExpenditureData
 */
public class ViewExpenditureSingleActivity extends AppCompatActivity {
    private TextView mDateText;
    private TextView mAmountText;
    private TextView mRecipientText;
    private TextView mCategoryText;
    private TextView mDescriptionText;
    private ImageView mReceiptImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_view_expenditure_single);
        setViews();

        //get expenditure data from previous intent
        Intent intent = getIntent();
        ExpenditureData expenditureData = (ExpenditureData) intent.getSerializableExtra(Constants.EXPENDITURE_DATA);
        setDisplayData(expenditureData);
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_expenditure_single, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            //show navigation drawer
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * sets views from XML layout
     */
    private void setViews() {
        mDateText = (TextView) findViewById(R.id.view_single_expenditure_date_text);
        mAmountText = (TextView) findViewById(R.id.view_single_expenditure_amount_text);
        mRecipientText = (TextView) findViewById(R.id.view_single_expenditure_recipient_text);
        mCategoryText = (TextView) findViewById(R.id.view_single_expenditure_category_text);
        mDescriptionText = (TextView) findViewById(R.id.view_single_expenditure_description_text);
        mReceiptImageView = (ImageView) findViewById(R.id.view_single_expenditure_photo_view);
    }

    /**
     * set text for TextViews from ExpenditureData
     *
     * @param expenditureData
     */
    private void setDisplayData(ExpenditureData expenditureData) {
        mDateText.setText(Constants.DATE_FORMAT.format(new Date(expenditureData.getDate())));
        mAmountText.setText(String.valueOf(Constants.CURRENCY_FORMAT.format(expenditureData.getAmount())));
        mRecipientText.setText(expenditureData.getRecipient());
        mCategoryText.setText(expenditureData.getCategory());
        mDescriptionText.setText(expenditureData.getDescription());

        Bitmap receiptImageBitmap = Methods.decodeSampledBitmapFromFile(expenditureData.getReceipt());
        mReceiptImageView.setImageBitmap(receiptImageBitmap);
    }

}
