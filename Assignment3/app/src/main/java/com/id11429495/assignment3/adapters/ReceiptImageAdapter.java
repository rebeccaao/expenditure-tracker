package com.id11429495.assignment3.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.id11429495.assignment3.Constants;
import com.id11429495.assignment3.Methods;
import com.id11429495.assignment3.model.ExpenditureCursor;
import com.id11429495.assignment3.model.ExpenditureData;
import com.id11429495.assignment3.model.ExpenditureDatabaseHelper;

/**
 * Adapter to display receipts based on file paths stored in the database
 */
public class ReceiptImageAdapter extends BaseAdapter {
    private Context mContext;
    private ExpenditureCursor mCursor;

    /**
     * constructor to fill adapter with file paths
     *
     * @param context
     */
    public ReceiptImageAdapter(Context context) {
        mContext = context;
        mCursor = ExpenditureDatabaseHelper.get(mContext, ExpenditureData.DATABASE_NAME).getAllExpendituresWithReceipts();
    }

    public int getCount() {
        return mCursor.getCount();
    }

    public ExpenditureData getItem(int position) {
        mCursor.moveToPosition(position);
        return mCursor.getExpenditureData();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * creates ImageView to display image of receipt stored at the file path
     *
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            //if the view does not exist, create it
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(Constants.RECEIPT_GRID_IMAGE_VIEW_WIDTH, Constants.RECEIPT_GRID_IMAGE_VIEW_HEIGHT));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = (ImageView) convertView;
        }

        //fill ImageView with image based on filepath from cursor
        ExpenditureData expenditureData = getItem(position);
        Bitmap cameraImage = Methods.decodeSampledBitmapFromFile(expenditureData.getReceipt());
        imageView.setImageBitmap(cameraImage);

        return imageView;
    }
}