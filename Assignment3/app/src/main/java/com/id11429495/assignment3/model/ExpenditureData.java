package com.id11429495.assignment3.model;

import android.database.Cursor;
import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Simple class containing data for Expenditures
 */
public class ExpenditureData implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String DATABASE_NAME = "expenditure.db";
    public static final String TABLE = "expenditure";
    public static final String COLUMN_ID = BaseColumns._ID;
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_RECIPIENT = "recipient";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_RECEIPTS = "receipts";
    public static final String COLUMN_ACCUMULATED_AMOUNT = "accumulated_amount";
    public static final String[] COLUMNS = {COLUMN_ID, COLUMN_DATE, COLUMN_AMOUNT, COLUMN_RECIPIENT, COLUMN_CATEGORY, COLUMN_DESCRIPTION, COLUMN_RECEIPTS};

    private int mId;
    private long mDate;
    private float mAmount;
    private String mRecipient;
    private String mCategory;
    private String mDescription;
    private String mReceiptFilepath;

    /**
     * Constructor to create ExpenditureData object with data
     *
     * @param id
     * @param date
     * @param amount
     * @param recipient
     * @param category
     * @param description
     * @param receiptFilePath
     */
    public ExpenditureData(int id, long date, float amount, String recipient, String category, String description, String receiptFilePath) {
        this.mId = id;
        this.mDate = date;
        this.mAmount = amount;
        this.mRecipient = recipient;
        this.mCategory = category;
        this.mDescription = description;
        this.mReceiptFilepath = receiptFilePath;
    }

    /**
     * Constructor to create a ExpenditureData object from a cursor
     *
     * @param cursor
     */
    public ExpenditureData(Cursor cursor) {
        this.mId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        this.mDate = cursor.getLong(cursor.getColumnIndex(COLUMN_DATE));
        this.mAmount = cursor.getFloat(cursor.getColumnIndex(COLUMN_AMOUNT));
        this.mRecipient = cursor.getString(cursor.getColumnIndex(COLUMN_RECIPIENT));
        this.mCategory = cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORY));
        this.mDescription = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
        this.mReceiptFilepath = cursor.getString(cursor.getColumnIndex(COLUMN_RECEIPTS));
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        this.mDate = date;
    }

    public float getAmount() {
        return mAmount;
    }

    public void setAmount(float amount) {
        this.mAmount = amount;
    }

    public String getRecipient() {
        return mRecipient;
    }

    public void setRecipient(String recipient) {
        this.mRecipient = recipient;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        this.mCategory = category;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getReceipt() {
        return mReceiptFilepath;
    }

    public void setReceipt(String receipt) {
        this.mReceiptFilepath = receipt;
    }
}
