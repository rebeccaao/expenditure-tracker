package com.id11429495.assignment3.model;


import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteQuery;

/**
 * Custom cursor factory to create instances of Expenditure Cursor
 */
public class ExpenditureCursor extends SQLiteCursor {
    public ExpenditureCursor(SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
        super(driver, editTable, query);
    }

    /**
     * cursor to provide ExpenditureData
     *
     * @return
     */
    public ExpenditureData getExpenditureData() {
        return new ExpenditureData(this);
    }

    /**
     * cursor to provide data for graph
     *
     * @return
     */
    public ExpenditureGraphData getExpenditureGraphData() {
        return new ExpenditureGraphData(this);
    }
}