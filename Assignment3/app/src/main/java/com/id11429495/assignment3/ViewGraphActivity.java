package com.id11429495.assignment3;

import android.content.res.Resources;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.id11429495.assignment3.model.ExpenditureCursor;
import com.id11429495.assignment3.model.ExpenditureData;
import com.id11429495.assignment3.model.ExpenditureDatabaseHelper;
import com.id11429495.assignment3.model.ExpenditureGraphData;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Activity to show graph data of amount spent on each category per day from database
 */
public class ViewGraphActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private LineChart mChart;
    private LineData mLineData;
    private long mEarliestDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_graph);
        setViews();
        mChart = (LineChart) findViewById(R.id.view_graph_line_chart);
        generateGraph();
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_graph, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * sets views from XML layout and sets OnClickListeners to buttons
     */
    private void setViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.view_graph_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.view_graph_nav_drawer);
        Methods.setNavigationDrawer(this, mDrawerLayout, mDrawerList);
    }

    /**
     * generate chart based on data from the database
     */
    private void generateGraph() {
        Resources resources = getApplicationContext().getResources();
        //check for data in the database first before trying to generate the graph
        boolean databaseHasData = !(ExpenditureDatabaseHelper.get(this, ExpenditureData.DATABASE_NAME).hasData());

        if (databaseHasData) {
            //get the date range to display in the graph
            int numberOfDays = getDateRange();
            //array for dates and entries for X-axis
            String[] dates = new String[numberOfDays];
            ArrayList<String> dateEntries = new ArrayList<>();
            for (int i = 0; i < numberOfDays; i++) {
                dateEntries.add(dates[i % dates.length]);
            }
            //array of amounts spent in a day for Y-axis
            float[] amounts = new float[numberOfDays];

            //ArrayList for all LineDataSets
            ArrayList<LineDataSet> dataSets = new ArrayList<>();

            //generate LineDataSet for each category based on categories in strings XML and add to ArrayList of data sets
            String[] categories = resources.getStringArray(R.array.categories_array);
            for (String category : categories) {
                LineDataSet lineDataSet = getLineDataSetByCategory(resources, numberOfDays, dates, amounts, category);
                dataSets.add(lineDataSet);
            }

            //create add the LineDataSets to the chart
            mLineData = new LineData(dateEntries, dataSets);
            mChart.setData(mLineData);
        }
        setChartStyles(databaseHasData);
        mChart.invalidate();
    }

    /**
     * calculate date range for the graph to display based on earliest and latest expenditure dates
     *
     * @return
     */
    private int getDateRange() {
        ExpenditureCursor c = ExpenditureDatabaseHelper.get(this, ExpenditureData.DATABASE_NAME).getAllExpenditures();
        final int extraDayBuffer = 1;
        ExpenditureData expenditureData = c.getExpenditureData();
        mEarliestDate = expenditureData.getDate();
        c.moveToLast();
        long mLatestDate = expenditureData.getDate();
        return Methods.getNumberOfDaysBetweenDates(mEarliestDate, mLatestDate) + extraDayBuffer;
    }

    /**
     * generate LineDataSet for specified category based on total amount spent on each day
     *
     * @param resources
     * @param numberOfDays
     * @param dates
     * @param amounts
     * @param category
     * @return
     */
    private LineDataSet getLineDataSetByCategory(Resources resources, int numberOfDays, String[] dates, float[] amounts, String category) {
        ExpenditureCursor c = ExpenditureDatabaseHelper.get(this, ExpenditureData.DATABASE_NAME).getExpendituresAmountByCategory(category);

        Calendar dateIterator = Calendar.getInstance();
        dateIterator.setTimeInMillis(mEarliestDate);

        // iterate through each day in the date range and add amount
        for (int i = 0; i < numberOfDays; i++) {
            if ((c.getCount() != 0) && dateIterator.getTimeInMillis() == c.getLong(c.getColumnIndex(ExpenditureData.COLUMN_DATE))) {
                ExpenditureGraphData graphData = c.getExpenditureGraphData();
                dates[i] = (Constants.SHORT_DATE_FORMAT.format(graphData.getDate()));
                amounts[i] = graphData.getTotalAmount();
                if (!c.isLast()) {
                    c.moveToNext();
                }
            } else {
                //set amount on graph to 0 if there is no data recorded for a specific date
                dates[i] = Constants.SHORT_DATE_FORMAT.format(dateIterator.getTimeInMillis());
                amounts[i] = 0;
            }
            dateIterator.add(Calendar.DATE, 1);
        }

        //create array of entries for amounts for Y-axis
        ArrayList<Entry> amountEntries = new ArrayList<>();
        for (int i = 0; i < numberOfDays; i++) {
            amountEntries.add(new Entry(amounts[i], i));
        }

        //create the LineDataSet and set the line color based on category
        LineDataSet lineDataSet = new LineDataSet(amountEntries, category);
        int categoryColorResId = resources.getIdentifier(category.toLowerCase(), "color", getApplicationContext().getPackageName());
        lineDataSet.setColor(resources.getColor(categoryColorResId));
        lineDataSet.setDrawCircles(false);
        return lineDataSet;
    }

    /**
     * set the visual styles for the line graph
     *
     * @param dataExists
     */
    private void setChartStyles(boolean dataExists) {
        if (dataExists) {
            mChart.setScaleMinima((float) mLineData.getXValCount() / 31f, 1f);
            mLineData.setDrawValues(false);
        }
        mChart.setDescription("");
        mChart.setNoDataTextDescription(String.valueOf(R.string.empty_graph));
        mChart.setScaleEnabled(true);
        mChart.setHighlightEnabled(false);
    }
}
