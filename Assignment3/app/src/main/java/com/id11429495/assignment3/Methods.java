package com.id11429495.assignment3;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.ikimuhendis.ldrawer.ActionBarDrawerToggle;
import com.ikimuhendis.ldrawer.DrawerArrowDrawable;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

/**
 * Global methods used by this application
 */
public class Methods {

    /**
     * create NavigationDrawer for activity
     *
     * @param appCompatActivity
     * @param drawerLayout
     * @param drawerList
     */
    public static void setNavigationDrawer(AppCompatActivity appCompatActivity, DrawerLayout drawerLayout, ListView drawerList) {
        final AppCompatActivity activity = appCompatActivity;
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);

        DrawerArrowDrawable drawerArrow = new DrawerArrowDrawable(activity) {
            @Override
            public boolean isLayoutRtl() {
                return false;
            }
        };

        // show drawer when icon is clicked
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(activity, drawerLayout, drawerArrow, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                activity.invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                activity.invalidateOptionsMenu();
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        Resources res = activity.getResources();
        //create array of options on the NavigationDrawer based on array in string xml
        String[] navigationDrawerArray = res.getStringArray(R.array.navigation_drawer_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, android.R.id.text1, navigationDrawerArray);
        drawerList.setAdapter(adapter);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                switch (position) {
                    case 0:
                        //start ViewCalendarActivity
                        intent = new Intent(activity.getApplicationContext(), ViewCalendarActivity.class);
                        activity.startActivity(intent);
                        break;
                    case 1:
                        //start ViewGraphActivity
                        intent = new Intent(activity.getApplicationContext(), ViewGraphActivity.class);
                        activity.startActivity(intent);
                        break;
                    case 2:
                        //start ViewReceiptsActivity
                        intent = new Intent(activity.getApplicationContext(), ViewReceiptsActivity.class);
                        activity.startActivity(intent);
                        break;
                }
            }
        });
    }

    /**
     * convert a string to long using Constants.DATE_FORMAT
     *
     * @param dateString
     * @return
     */
    public static long getDateInMilliseconds(String dateString) {
        Calendar cal = Calendar.getInstance();

        try {
            cal.setTime(Constants.DATE_FORMAT.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return cal.getTimeInMillis();
    }

    /**
     * check if EditText contains text.
     *
     * @param editText
     * @return
     */
    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {
            editText.setError(Constants.REQUIRED_MSG);
            return false;
        }
        return true;
    }

    /**
     * calculate amount of days between two longs
     *
     * @param earliestDate
     * @param latestDate
     * @return
     */
    public static int getNumberOfDaysBetweenDates(long earliestDate, long latestDate) {
        final long DAY_IN_MILLIS = 1000 * 24 * 60 * 60;
        long timeBetween = latestDate - earliestDate;
        return (int) ((timeBetween / DAY_IN_MILLIS) + 1);
    }

    /**
     * decode the bitmap from the specified file and return
     *
     * @param filename
     * @return
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename) {
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        int height = (bitmap.getHeight() * Constants.RECEIPT_IMAGE_VIEW_PREVIEW / bitmap.getWidth());
        bitmap = Bitmap.createScaledBitmap(bitmap, Constants.RECEIPT_IMAGE_VIEW_PREVIEW, height, true);

        //check the orientation of bitmap and display it correctly
        ExifInterface ei;
        try {
            ei = new ExifInterface(filename);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateBitmap(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateBitmap(bitmap, 180);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * rotate the bitmap
     *
     * @param source
     * @param angle
     * @return
     */
    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
