package com.id11429495.assignment3;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Global constants used by the application
 */
public final class Constants {
    public static final String TAG = "ASS3";

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM");
    public static final DecimalFormat CURRENCY_FORMAT = new DecimalFormat("##0.00");

    public static final String EXPENDITURE_DATA = "ExpenditureData";
    public static final String SELECTED_DATE = "SelectedDate";

    public static final int ADD_ACTIVITY_REQUEST_CODE = 1;
    public static final int REQUEST_IMAGE_CAPTURE = 2;

    public static final long DEFAULT_LONG_VALUE = 0;

    public static final String REQUIRED_MSG = "Input is required";

    public static final int RECEIPT_GRID_IMAGE_VIEW_WIDTH = 100;
    public static final int RECEIPT_GRID_IMAGE_VIEW_HEIGHT = 133;
    public static final int RECEIPT_IMAGE_VIEW_PREVIEW = 512;
}
