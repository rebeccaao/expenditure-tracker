package com.id11429495.assignment3;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ListView;

import com.id11429495.assignment3.adapters.ReceiptImageAdapter;

/**
 * Activity to view all receipts created and stored in the application's database
 */
public class ViewReceiptsActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_receipts);
        setViewsListenersAndAdapter();
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_calendar, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            //show navigation drawer
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * sets NavigationDrawer
     * sets GridView from XML layout and sets adapter for displaying receipts
     */
    private void setViewsListenersAndAdapter() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.view_calendar_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.view_calendar_nav_drawer);
        Methods.setNavigationDrawer(this, mDrawerLayout, mDrawerList);

        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ReceiptImageAdapter(this));
    }


}