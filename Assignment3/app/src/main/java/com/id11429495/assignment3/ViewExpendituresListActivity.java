package com.id11429495.assignment3;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.*;
import android.widget.ListView;

import com.id11429495.assignment3.adapters.ListExpendituresByDateAdapter;
import com.melnykov.fab.FloatingActionButton;

import java.util.Calendar;

/**
 * Activity to show the ExpenditureData from the database based on the date
 */
public class ViewExpendituresListActivity extends AppCompatActivity implements OnClickListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    private long mSelectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_expenditures_list);
        setSelectedDate();
        setViewsListenersAndAdapter();
    }

    /**
     * inflates options menu and adds to the action bar
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_expenditures_list, menu);
        return true;
    }

    /**
     * calls methods for clicking on options in the menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        int id = item.getItemId();
        switch (id) {
            //show navigation drawer
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;
            //show calendar to pick a new date
            case R.id.view_expenditures_list_menu_show_calendar:
                intent = new Intent(getApplicationContext(), ViewCalendarActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * set selected date from previous activity
     */
    private void setSelectedDate() {
        Intent intent = getIntent();
        mSelectedDate = intent.getLongExtra(Constants.SELECTED_DATE, Constants.DEFAULT_LONG_VALUE);
        String formattedDate;
        if (mSelectedDate == 0) {
            Calendar cal = Calendar.getInstance();
            formattedDate = Constants.DATE_FORMAT.format(cal.getTime());
            mSelectedDate = Methods.getDateInMilliseconds(formattedDate);
        } else {
            formattedDate = Constants.DATE_FORMAT.format(mSelectedDate);
        }
        //set action bar title to selected date
        setTitle(String.format(getResources().getString(R.string.title_activity_view_expenditures_by_date), formattedDate));
    }

    /**
     * sets views from XML layout, sets OnClickListeners to buttons, and sets adapters to display data
     */
    private void setViewsListenersAndAdapter() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.view_expenditures_list_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.view_expenditures_list_nav_drawer);
        Methods.setNavigationDrawer(this, mDrawerLayout, mDrawerList);

        //button to add new ExpenditureData
        FloatingActionButton addExpenditureButton = (FloatingActionButton) findViewById(R.id.add_expenditure_floating_button);
        addExpenditureButton.setOnClickListener(this);

        //adapter to display data for selected date
        ListExpendituresByDateAdapter listExpendituresByDateAdapter = new ListExpendituresByDateAdapter(this, mSelectedDate);
        ListView expendituresByDayView = (ListView) findViewById(R.id.view_list_expenditures_by_day_list);
        expendituresByDayView.setEmptyView(findViewById(R.id.view_list_expenditures_empty));
        expendituresByDayView.setAdapter(listExpendituresByDateAdapter);
    }

    /**
     * handle OnClickListener button clicks
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.add_expenditure_floating_button:
                intent = new Intent(getApplicationContext(), AddExpenditureActivity.class);
                startActivityForResult(intent, Constants.ADD_ACTIVITY_REQUEST_CODE);
                break;
        }
    }

    /**
     * handles methods for after an startActivityForResult is executed
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //refresh activity to show newly added expenditure on the correct date page
        if (requestCode == Constants.ADD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, ViewExpendituresListActivity.class);
            intent.putExtra(Constants.SELECTED_DATE, data.getLongExtra(Constants.SELECTED_DATE, Constants.DEFAULT_LONG_VALUE));
            startActivity(intent);
        }
    }
}
